package com.example.fcmnotif;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by malikeh on 2017-09-17.
 */

public class FirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseMsgService";
    private RemoteViews remoteViews;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage.getData().get("body"), remoteMessage.getData().get("title"));
           // sendNotification(remoteMessage.getData().get("hi"),"salam");//test
        }

//        else if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
//        }

    }

    private void sendNotification(String body, String title) {


        remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);

        remoteViews.setTextViewText(R.id.title, "Firebase");
        remoteViews.setTextViewText(R.id.body, body);
        remoteViews.setImageViewResource(R.id.icon, R.mipmap.ic_launcher);

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );//| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0, new Intent[]{intent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setCustomContentView(remoteViews)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                .setContentTitle(title)
                .setContentText(body)
                .setCustomHeadsUpContentView(remoteViews)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setTicker(body)
                .setSound(notificationSound)
                //.setFullScreenIntent(pendingIntent, true)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            notifBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

        Log.d(TAG, " MSG"+body);
        if (Build.VERSION.SDK_INT >= 21){
            notifBuilder.setVibrate(new long[0]);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), notifBuilder.build());
    }
}
